<?php

namespace App\Http\Controllers;

use App\Employees;
use Illuminate\Http\Request;
use DB;

class EmployeesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         $employees = Employees::latest()->paginate(5);

        //echo json_encode($employees); exit;

        return view('employees.index',compact('employees'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('employees.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name'    =>  'required',
            'email'     =>  'required|email|unique:employees',
            'file_upload'     =>  'required',
            'joining_date' => 'required',
            'known_technologies' => 'required|min:3'
        ]);

        $file_upload = $request->file('file_upload');

        $new_name = rand() . '.' . $file_upload->getClientOriginalExtension();
        $file_upload->move(public_path('images'), $new_name);

        $employees = new Employees();

        $employees->name = $request->name;
        $employees->email = $request->email;
        $imp = implode(", ", (array) $request->known_technologies);       

        $employees->known_technologies = $imp;
        $employees->joining_date = $request->joining_date;
        $employees->file_upload = $new_name;

       
        $employees->save();        

        return redirect('employees')->with('success', 'Employees Added successfully.');


    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Employees  $employees
     * @return \Illuminate\Http\Response
     */
    public function show(Employees $employees,$id)

    {
        $data = Employees::findOrFail($id);
        return view('employees.show',compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Employees  $employees
     * @return \Illuminate\Http\Response
     */
    public function edit(Employees $employees, $id)
    {
        $data = Employees::findOrFail($id);

       
        return view('employees.edit',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Employees  $employees
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Employees $employees, $id)
    {
        $image_name = $request->hidden_image;
        $image = $request->file('file_upload');

        if($image != '')
        {

        $request->validate([
            'name' => 'required',
            'email' => 'required',            
            'joining_date' => 'required',
            'known_technologies' => 'required|min:3'
        ]);
        $image_name = rand() . '.' . $image->getClientOriginalExtension();
        $image->move(public_path('images'), $image_name);
    }
    else{

        $request->validate([
                'name'    =>  'required',
                'email'     =>  'required',                
            'joining_date' => 'required',
            'known_technologies' => 'required|min:3'
            ]);

    }



    $form_data = array(
            'name'               =>   $request->name,
            'email'              =>   $request->email,
            'known_technologies' =>   implode(',',$request->known_technologies),
            'joining_date'       =>   $request->joining_date,
            'file_upload'        =>   $image_name
        );

    Employees::whereId($id)->update($form_data);

    return redirect('employees')->with('success', 'Employees Update successfully.');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Employees  $employees
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Employees::findOrFail($id);
        $data->delete();

        return redirect('employees')->with('success', 'Employees successfully deleted.');
    }

    public function checkEmail(Request $request)
    {
     if($request->get('email'))
     {
      $email = $request->get('email');
      $data = DB::table("employees")
       ->where('email', $email)
       ->count();
      if($data > 0)
      {
       echo 'not_unique';
      }
      else
      {
       echo 'unique';
      }
     }
    }
}
