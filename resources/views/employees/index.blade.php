@extends('employees.layout')
 
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
           <!--  <div class="pull-left">
                <h2>Employees Management System</h2>
            </div> -->
            <br>
            <div class="pull-right">
                <a class="btn btn-success" href="{{ route('employees.create') }}"> Create New Employees</a>
            </div>
        </div>
    </div>
    <br>
   
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
   
    <table class="table table-bordered">
        <tr>
           <th>SNO</th>
            <th>Name</th>
            <th>Email</th>
           
            <th>Joining Date</th>
            <th>Known Technologies</th>
            <th width="280px">Action</th>
        </tr>
        @foreach ($employees as $employee)
        <tr>
            <td>{{ $loop->iteration }}</td>
            <td>{{ $employee->name }}</td>
            <td>{{ $employee->email }}</td>           
            <td>{{ $employee->joining_date }}</td>
            <td> @php $conut =  $employee->known_technologies; $count1 = explode(',', $conut); for($i=0;$i < count($count1); $i++){  if($count1[$i] == "1") {  echo 'PHP,'; } else if ($count1[$i] == "2") { echo 'Javascript,'; } else if($count1[$i] == "3") { echo 'AJAX,'; } else if($count1[$i] == "4") { echo '.NET,'; } else if($count1[$i] == "5") { echo 'Java'; } else {}  } @endphp </td>
            <td>
                <form action="{{ route('employees.destroy',$employee->id) }}" method="POST">
   
                    <a class="btn btn-info" href="{{ route('employees.show',$employee->id) }}">Show</a>
    
                    <a class="btn btn-primary" href="{{ route('employees.edit',$employee->id) }}">Edit</a>
   
                    @csrf
                    @method('DELETE')
      
                    <button type="submit" class="btn btn-danger">Delete</button>
                </form>
            </td>
        </tr>
        @endforeach
    </table>
  
{!! $employees->links() !!}
      
@endsection