@extends('employees.layout')

@section('content')
            
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="pull-left">
                <h4>Edit Employees</h4>
            </div> 
            <div align="right">
                <a href="{{ route('employees.index') }}" class="btn btn-warning">Back</a>
            </div>
            <br />
     <form method="post" action="{{ route('employees.update', $data->id) }}" enctype="multipart/form-data" id ="form">
                @csrf
                @method('PATCH')
                
            <div class="row"> 
              

                <div class="col-sm-12">
                  <div class="col-sm-6">

      <div class="form-group">
       <label class="col-md-4 text-right">Name</label>
       <div class="col-md-8">
        <input type="text" name="name" value="{{ $data->name }}" class="form-control" />
       </div>
      </div>
    </div>
           
    <div class="col-sm-6">
      <div class="form-group">
       <label class="col-md-4 text-right">Email</label>
       <div class="col-md-6"> 
        <input type="text" name="email" value="{{ $data->email }}" class="form-control" />
       </div>
      </div>
    </div>
  </div>
</div>
      <br />
      <br />
      <br />
      <div class="row"> 
              

                <div class="col-sm-12">
                  <div class="col-sm-6">
       <div class="form-group">
       <label class="col-md-4 text-right">Joining Date</label>
       <div class="col-md-6">
        <input type="text" id="datepicker" name="joining_date" value="{{ $data->joining_date }}" class="form-control" />
       </div>
      </div>
    </div>
    <div class="col-sm-6">
       <div class="form-group">
       <label class="col-md-4 text-right">Uplode File</label>
       <div class="col-md-8">
        <input type="file" name="file_upload" />
        <a href="{{ URL::to('/') }}/images/{{ $data->file_upload }}" download="true">{{ $data->file_upload }}</a>
              
                        <input type="hidden" name="hidden_image" value="{{ $data->file_upload }}" />
       </div>
      </div>
      
    </div>
     
      
 </div>
 </div>
 </div> 
 <br />
      <br />
      <br />
      <div class="row">
        <div class="col-sm-12">
           <div class="form-group">
  <label class="col-md-4 text-right">Known Technologies</label>
  <div class="col-md-6">

       
   <input name="known_technologies[]" type="checkbox"  @php $conut =  $data->known_technologies; $count1 = explode(',', $conut); for($i=0;$i < count($count1); $i++){  if($count1[$i] == "1") {  echo 'checked'; } else { echo ''; }  } @endphp > PHP
  <input name="known_technologies[]" type="checkbox" value="2"  @php $conut =  $data->known_technologies; $count1 = explode(',', $conut); for($i=0;$i < count($count1); $i++){  if($count1[$i] == "2") {  echo 'checked'; } else { echo ''; } } @endphp > Javascript
  <input name="known_technologies[]" type="checkbox" value="3"  @php $conut =  $data->known_technologies; $count1 = explode(',', $conut); for($i=0;$i < count($count1); $i++){  if($count1[$i] == "3") {  echo 'checked'; } else { echo ''; } } @endphp > Ajax
  <input name="known_technologies[]" type="checkbox" value="4"  @php $conut =  $data->known_technologies; $count1 = explode(',', $conut); for($i=0;$i < count($count1); $i++){  if($count1[$i] == "4") {  echo 'checked'; } else { echo ''; } } @endphp > .NET
  <input name="known_technologies[]" type="checkbox" value="5"  @php $conut =  $data->known_technologies; $count1 = explode(',', $conut); for($i=0;$i < count($count1); $i++){  if($count1[$i] == "5") {  echo 'checked'; } else { echo ''; } } @endphp  > Java
  
    </div>
 </div>
</div>
</div>
     
      <br /><br /><br />
      <div class="form-group text-center">
       <input type="submit" name="edit" class="btn btn-primary " value="Update" />
       <a href="{{ route('employees.index') }}" class="btn btn-danger">Cancel</a>
      </div>
    </div>
 
     </form>
     <script>
$(document).ready(function(){

 $('#email').blur(function(){
  var error_email = '';
  var email = $('#email').val();
  //alert(email);
  var _token = $('input[name="_token"]').val();
  var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
  if(!filter.test(email))
  {    
   $('#error_email').html('<label class="text-danger">Invalid Email</label>');
   $('#email').addClass('has-error');
  
  }
  else
  {
   $.ajax({
    url:"{{ route('email_available.check') }}",
    method:"POST",
    data:{email:email, _token:_token},
    success:function(result)
    {
     if(result == 'unique')
     {
      $('#error_email').html('<label class="text-success">Email is correct</label>');
      $('#email').removeClass('has-error');
      
     }
     else
     {
      $('#error_email').html('<label class="text-danger">Email Is Already Taken</label>');
      $('#email').addClass('has-error');
      
     }
    }
   })
  }
 });
 
});
</script>
     <script>
 
    $(document).ready(function () {
 
    $('#form').validate({ // initialize the plugin
        rules: {
            name: {
                required: true
            },
            email: {
                required: true,
                email: true
            },
            joining_date: {
                required: true,
                joining_date: true
            }, 
            file_upload: {
                required: true,
                file_upload:true
              },
            known_technologies: {
              required: true,
              known_technologies:true,
              min:3

            },         
            
        }
    });
});
</script>

@endsection