<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employees extends Model
{
	protected $table = 'employees';
	
	protected $fillable = [
        'name', 
        'email',
        'known_technologies',
        'joining_date',
        'file_upload',
    ];
}
