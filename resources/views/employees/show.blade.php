@extends('employees.layout')

@section('content')
            
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
             <div class="pull-left">
                <h4>Show Employees</h4>
            </div> 
            <div align="right">
                <a href="{{ route('employees.index') }}" class="btn btn-warning">Back</a>
            </div>
            <br />
<div class="container">
            <div class="row"> 
              

                <div class="col-sm-12">
                  <div class="col-sm-6"> 
                    <div class="form-group">
       <label class="col-md-4 ">Name</label>
       <div class="col-md-8">
        <input type="text" name="name" value="{{ $data->name }}" class="form-control"  readonly="" />
       </div>
      </div></div>
                   <div class="col-sm-6">
                       <div class="form-group">
       <label class="col-md-4 text-right">Email</label>
       <div class="col-md-6">
        <input type="text" name="email" value="{{ $data->email }}" class="form-control"  readonly=""/>
       </div>
      </div>
      </div>
                  
     </div>
   </div>

      <br />
      <br />
      <br />
       
      


    <div class="row"> 
      <div class="col-sm-12">
         <div class="col-sm-6">
           <div class="form-group">
       <label class="col-md-4 text-right">Joining Date</label>
       <div class="col-md-8">
        <input type="text" id="datepicker" name="joining_date" value="{{ $data->joining_date }}" class="form-control" readonly=""/>
       </div>
      </div>
         </div>
          <div class="col-sm-6">

 <div class="form-group">
       <label class="col-md-4 text-right">Uplode File</label>
       <div class="col-md-8">
        <input type="file" name="file_upload" readonly=""/>
        <a href="{{ URL::to('/') }}/images/{{ $data->file_upload }}" download="true">{{ $data->file_upload }}</a>
              
                        <input type="hidden" name="hidden_image" value="{{ $data->file_upload }}" />
       </div>
      </div>

           
          </div>
        
      </div>
    </div>
     <br />
      <br />
      <br />
       

    <div class="row">
      <div class="col-sm-12">
        <div class="form-group">
  <label class="col-sm-6 text-right">Known Technologies</label>
  <div class="col-md-6">


   
  <input name="known_technologies[]" type="checkbox" style="pointer-events:none;"  @php $conut =  $data->known_technologies; $count1 = explode(',', $conut); for($i=0;$i < count($count1); $i++){  if($count1[$i] == "1") {  echo 'checked'; } else { echo ''; }  } @endphp > PHP
  <input name="known_technologies[]" type="checkbox" value="2" style="pointer-events:none;" @php $conut =  $data->known_technologies; $count1 = explode(',', $conut); for($i=0;$i < count($count1); $i++){  if($count1[$i] == "2") {  echo 'checked'; } else { echo ''; } } @endphp > Javascript
  <input name="known_technologies[]" type="checkbox" value="3" style="pointer-events:none;"  @php $conut =  $data->known_technologies; $count1 = explode(',', $conut); for($i=0;$i < count($count1); $i++){  if($count1[$i] == "3") {  echo 'checked'; } else { echo ''; } } @endphp > Ajax
  <input name="known_technologies[]" type="checkbox" value="4" style="pointer-events:none;"  @php $conut =  $data->known_technologies; $count1 = explode(',', $conut); for($i=0;$i < count($count1); $i++){  if($count1[$i] == "4") {  echo 'checked'; } else { echo ''; } } @endphp > .NET
  <input name="known_technologies[]" type="checkbox" value="5" style="pointer-events:none;"  @php $conut =  $data->known_technologies; $count1 = explode(',', $conut); for($i=0;$i < count($count1); $i++){  if($count1[$i] == "5") {  echo 'checked'; } else { echo ''; } } @endphp  > Java

    </div>
 </div> 
    </div>
  </div>

             
            
     
    
      
 
     
      <br /><br /><br />
      <div class="form-group text-center">
        <a href="{{ route('employees.index') }}" class="btn btn-danger">Cancel</a>
      </div>
    </div>
  </div>

@endsection