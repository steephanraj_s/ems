select distinct act.accommodation_type_name,
art.accommodation_room_type_name,pk.park_name,ac.rooms - ((select count(no_of_rooms) from booking.booking where booking_type='accommodation' 
and bk.accommodation_room_type_id=ac.accommodation_room_type_id 
and bk.accommodation_type_id=ac.accommodation_type_id)) as booked_rooms,ac.start_date,ac.end_date from master.accommodation ac
left join master.accommodation_type act on (act.id = ac.accommodation_type_id)
left join master.accommodation_room_type as art on (art.id = ac.accommodation_room_type_id)
left join master.parks as pk on (pk.id = ac.park_id)
left join booking.booking as bk on (bk.accommodation_room_type_id = art.id and bk.accommodation_type_id = ac.accommodation_type_id)
where ac.status= 0 and ac.park_id = 33 and '2020-03-12' between ac.start_date and ac.end_date;

select * from booking.booking b where booking_type = 'accommodation';

