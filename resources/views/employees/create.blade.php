@extends('employees.layout')

@section('content')
@if($errors->any())
<div class="alert alert-danger">
 <ul>
  @foreach($errors->all() as $error)
  <li>{{ $error }}</li>
  @endforeach
 </ul>
</div>
@endif
<div align="right">
 <a href="{{ route('employees.index') }}" class="btn btn-default">Back</a>
</div>

<form method="post" action="{{ route('employees.store') }}" enctype="multipart/form-data" id="form">

 @csrf
 <div class="row">
  <div class="col-md-12">
  <div class="col-md-6">
 <div class="form-group">
  <label class="col-md-4 text-right">Employee Name</label>
  <div class="col-md-6">
   <input type="text" name="name" class="form-control"  placeholder="enter employee name" />
  </div>
 </div>
</div>
<div class="col-md-6">

 <div class="form-group">
  <label class="col-md-4 text-right">Employee Email</label>
  <div class="col-md-6">
   <input type="text" name="email" id="email" class="form-control" placeholder="enter employee email" />
   <span id="error_email"></span>
  </div>
 </div>
</div>
</div>
</div>

<br>
<br>


<div class="row">
  <div class="col-md-12">
  <div class="col-md-6">
 <div class="form-group">
  <label class="col-md-4 text-right">Joining Date</label>
  <div class="col-md-6">
   <input type="text" id="datepicker" name="joining_date" class="form-control" />
  </div>
 </div>
</div>
<div class="col-md-6">

 <div class="form-group">
  <label class="col-md-4 text-right">Upload a File</label>
  <div class="col-md-6">
   <input type="file" name="file_upload" />
  </div>
 </div>
</div>
</div>
</div>

<br>
<br>
<div class="row">
  <div class="col-md-12">
 <div class="form-group">
  <label class="col-md-4 text-right">Known Technologies</label>
  <div class="col-md-6">
   <input name="known_technologies[]" type="checkbox" value="1">PHP
  <input name="known_technologies[]" type="checkbox" value="2"> Javascript
  <input name="known_technologies[]" type="checkbox" value="3"> Ajax
  <input name="known_technologies[]" type="checkbox" value="4"> .NET
  <input name="known_technologies[]" type="checkbox" value="5"> Java
  </div>
 </div>    
  </div>
</div>
<br>
<br>

 <div class="form-group text-center">
  <input type="submit" name="add" class="btn btn-primary input-lg" value="Add" />
 </div>

</form>

<script>
$(document).ready(function(){

 $('#email').blur(function(){
  var error_email = '';
  var email = $('#email').val();
  //alert(email);
  var _token = $('input[name="_token"]').val();
  var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
  if(!filter.test(email))
  {    
   $('#error_email').html('<label class="text-danger">Invalid Email</label>');
   $('#email').addClass('has-error');
  
  }
  else
  {
   $.ajax({
    url:"{{ route('email_available.check') }}",
    method:"POST",
    data:{email:email, _token:_token},
    success:function(result)
    {
     if(result == 'unique')
     {
      $('#error_email').html('<label class="text-success">Email is correct</label>');
      $('#email').removeClass('has-error');
      
     }
     else
     {
      $('#error_email').html('<label class="text-danger">Email Is Already Taken</label>');
      $('#email').addClass('has-error');
      
     }
    }
   })
  }
 });
 
});
</script>

<script>
 
    $(document).ready(function () {
 
    $('#form').validate({ // initialize the plugin
        rules: {
            name: {
                required: true
            },
            email: {
                required: true,
                email: true
            },
            joining_date: {
                required: true,
                joining_date: true
            }, 
            file_upload: {
                required: true,
                file_upload:true
              },
            known_technologies: {
              required: true,
              known_technologies:true,
              min:3

            },         
            
        }
    });
});
</script>


@endsection



